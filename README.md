Solidarity is a verb, an action. 

It grows from the belief in collective liberation. Our movements are growing and more easily coordinating with one another to live out these values of solidarity, but all too often we’re dependent on platforms and tools that don’t have our best interests in mind.

We use platforms like Facebook to share video, only to have live feeds taken down. We organize events only for this data to be mined by police departments. We share our viewpoints and opinions, but at the cost of our movements and activities (even long after we logged out of facebook) to be tracked and sold to advertising agencies. As leaks and exposes repeatedly demonstrate- Facebook, Twitter, Snapchat, Instagram - they all regularly collude with intelligence agencies to monitor and disrupt us.

Solidarity is a distribution built by and for those of us conspiring, collaborating, supporting and resisting together. It will always be open source and will never sell your data or share your information with a government agency.

Solidarity starts with a core feature set that enables a group to highlight the sites of on the ground struggle and affiliated support groups through project mapping. When a particular site of struggle becomes active, its status is elevated and supporters are notified. Combining news feeds, resources and hashtags a group can easily communicate successes on the ground and needs that appear.

Because points of struggle and needs shift, Solidarity can quickly accommodate the changing terrain of resistance. One site can be set to stable while another promoted to “in need” and another to “actively resisting.” An on site donation tool empowers groups to fundraise on their own terms, ending our dependence on platforms such as GoFundMe or IndieGogo.

Solidarity also leverages the power of support groups and individuals effectively. This centers around the concept of a Call to Action. These are specific calls for people to support on the ground efforts. Supporters create accounts on the site and choose the types of actions they can follow through with. Networks now have the ability to coordinate and mobilize supporters in strategic ways. For example, all members committed to actions of accountability can be called upon to flood the phone lines of a prison that is denying a political prisoner access to medical treatment. A campaign may be up against a corporate media blackout of important events unfolding. A call to action sent out to social media supporters can set off a wave of tweets and posts that force the hand of mainstream media outlets.

Solidarity is launching in partnership with the Incarcerated Workers Organizing Committee, the prison union largely responsible for coordinating the largest prison strike ever.

We will begin with the most essential features so that this technology can be in the hands of activists as soon as possible. Then, based on crowdfunding and developer interest we will build upon the initial release to continue with regular add ons and improvements.

To begin with, Solidarity will launch with 

* Activity Mapping showing:
* Sites of struggle
* Solidarity groups
* On site fundraising
* Calls to Action


Once our first phase is launched we will explore further features, including:
* Membership accounts that can-
  * Share what calls to action a user took
  * Configure announcements a user receives
  * SMS integration